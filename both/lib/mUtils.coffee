#test commit


moment = require('moment')  #add momentjs:moment

if Meteor.isServer
  @CollectionError = new Mongo.Collection 'error' #Collection for error logging

  Meteor.methods
    'console.log': (msg) ->
      msg = "### #{if __file? then __file}/#{if __function? then __function}/#{if __line? then __line} $$$ #{msg}"
      console.log msg
    'throwError': (err) -> # err: thrown err object
      if typeof err is 'string'
        err = strMessage: err
      err.createdAt = new Date()
      err.__filename = __filename
      err.__function = __function
      err.__line = __line
      CollectionError.insert err

### Debug Info Usage
  err.createdAt = new Date()
  err.strMessage = err.toString()
  err.__filename = __filename
  err.__function = __function
  err.__line = __line
###
Object.defineProperty global, '__stack', get: ->
  orig = Error.prepareStackTrace

  Error.prepareStackTrace = (_, stack) ->
    stack

  err = new Error
  Error.captureStackTrace err, arguments.callee
  stack = err.stack
  Error.prepareStackTrace = orig
  stack
Object.defineProperty global, '__line', get: ->
  __stack[1].getLineNumber()
Object.defineProperty global, '__function', get: ->
  __stack[1].getFunctionName()

@isTestMode = do -> if process?.env?.IS_TEST_MODE is 'true' then true else false

@cl = (msg) ->
    console.log msg

# Date prototyping
@Date.prototype.addSeconds = (s) ->
  @setSeconds @getSeconds() + s
  return @
@Date.prototype.addMinutes = (m) ->
  @setMinutes @getMinutes() + m
  return @
@Date.prototype.addHours = (h) ->
  @setHours @getHours() + h
  return @
@Date.prototype.addDates = (d) ->
  @setDate @getDate() + d
  return @
@Date.prototype.addMonths = (value) ->
  n = @getDate()
  @setDate 1
  @setMonth @getMonth() + value
  @setDate Math.min(n, @getDaysInMonth())
  return @

Date.isLeapYear = (year) ->
  year % 4 == 0 and year % 100 != 0 or year % 400 == 0
Date.getDaysInMonth = (year, month) ->
  [
    31
    if Date.isLeapYear(year) then 29 else 28
    31
    30
    31
    30
    31
    31
    30
    31
    30
    31
  ][month]
@Date.prototype.isLeapYear = ->
  Date.isLeapYear @getFullYear()
@Date.prototype.getDaysInMonth = ->
  Date.getDaysInMonth @getFullYear(), @getMonth()
Date.prototype.clone = -> return new Date @getTime()

#todo .coffee runs before .js (not sure only for jUtils or not)
@CheckTimer = class
  ### jwjin/1508210653
  checkTime = new CheckTimer()
  ...logic...
  checkTime.log 'check place 1'
  ...logic...
  checkTime.log 'check place 2'
  ...
  This will display a time taken by logic as it's name
  ###

  constructor: -> @lastTime = new Date()
  log: (name) ->
    now = new Date()
    cl "#{name}: #{now.getTime() - @lastTime.getTime()}"
    @lastTime = now
    return
#throw Error with log on DB
@throwError = (err) ->
  Meteor.call 'throwError', err #err object or string for message


@mDefine =
  fileServerMark: '_-_-_fileServerUrl_-_-_'
  timeFormat: 'YYYY-MM-DD HH:mm:ss'
  timeFormatMDHM: 'MM-DD HH:mm'
  timeFormatMD: 'MM-DD'
  timeFormatYM: 'YYYY-MM'
  timeFormatYMD: 'YYYY-MM-DD'
  timeFormatYMDdot: 'YYYY.MM.DD'
  timeFormatHMS: 'HH:mm:ss'
  timeFormatH: 'HH'
  timeFormatM: 'mm'
  timeFormatHM: 'HH:mm'
## below is trash
  분류: {
    a1: '이사장 인사말'
    a2: '한반도 평화 만들기는'
    a3: '연혁'
    a4: '정관'
    a5: '구성원'
    a6: '조직도'
    b1: '회원동정'
    b2: '칼럼'
    b3: '기사'
    b4: '갤러리'
    c1: '한반도포럼'
    d1: '2015 평화오디세이'
    d2: '2016 평화오디세이'
    d3: '2016 청년오디세이'
    d4: '1090 평화와 통일운동'
    e1: '재무보고'
  }


@mUtils = {}
if Meteor.isServer
  _.extend @mUtils,
    convertBase64ToFileUrl: (_html) ->
      # html 텍스트를 받아서 base64 이미지만 추려서 업로드하고 url로 변경해서 넣은 후 리턴
      cheerio = require 'cheerio'
      $ = cheerio.load _html
      $('img').each (idx, img) ->
        #    $(내용).find('img').each (idx, img) ->
        baseStr = $(img).attr 'src'
        file = mUtils.convertBase64ToBinary baseStr
        if file is 'notBase64' then return
        fileInfo =
          name: ''  #base64라서 이름이 없다.
          size: file.binary.length
          type: file.type
          dir: settings.public.fileServerDir

        fileServer = DDP.connect settings.public.fileServerIpPort
        fut = new (require('fibers/future'))()
        fileServer.call 'upload', fileInfo ,file.binary, (err, rslt) ->
          if err then alert err
          fut.return rslt.path
        tmpPath = fut.wait()
        $(img).attr 'src', "#{mDefine.fileServerMark}#{tmpPath}"
      return $('body').html()

    convertBase64ToBinary: (dataURI) ->
      BASE64_MARKER = ';base64,'
      base64Index = dataURI.indexOf(BASE64_MARKER)
      if base64Index is -1 then return 'notBase64'
      base64Index += BASE64_MARKER.length
      base64 = dataURI.substring(base64Index)
      type = dataURI.substring 0, base64Index
      type = type.split('/')[1].split(';')[0]

      raw = require('atob')(base64)
      rawLength = raw.length
      array = new Uint8Array(new ArrayBuffer(rawLength))
      i = 0
      while i < rawLength
        array[i] = raw.charCodeAt(i)
        i++
      return {
      binary: array
      type: type
      }

_.extend @mUtils,
  getStringYMDFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatYMD)
  getStringYMDdotFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatYMDdot)
  getStringMDHMFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatMDHM)
  getStringMDFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatMD)
  getStringHMSFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatHMS)
  getStringHFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatH)
  getStringMFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatM)
  getStringHMFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormatHM)
  getStringYMDHMSFromDate: (_date) ->
    return moment(_date).format(mDefine.timeFormat)
  getDateFromString: (_date) ->
    return moment(_date, mDefine.timeFormat).toDate()

  replaceAll: (_string, _selector, _any) ->
    cl revTest = _string.replace(/_selector/gi, _any);
    return revTest

  formatBytes: (bytes, decimals) ->
#    usage: formatBytes(139328839)
    if(bytes == 0) then return '0 Byte'
    k = 1000
    dm = decimals + 1 || 3
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    i = Math.floor(Math.log(bytes) / Math.log(k))
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]

  mydiff: (date1, date2, interval) ->
    #usage: mydiff('date1', 'date2', 'days')
    second = 1000
    minute = second * 60
    hour = minute * 60
    day = hour * 24
    week = day * 7
    date1 = new Date(date1)
    date2 = new Date(date2)
    timediff = date2 - date1
    if isNaN(timediff)
      return NaN
    switch interval
      when 'years'
        return date2.getFullYear() - date1.getFullYear()
      when 'months'
        return date2.getFullYear() * 12 + date2.getMonth() - (date1.getFullYear() * 12 + date1.getMonth())
      when 'weeks'
        return Math.floor(timediff / week)
      when 'days'
        return Math.floor(timediff / day)
      when 'hours'
        return Math.floor(timediff / hour)
      when 'minutes'
        return Math.floor(timediff / minute)
      when 'seconds'
        return Math.floor(timediff / second)
      else
        return undefined


  cordovaDeviceType: ->
    if navigator.userAgent.match(/iPad/i) or navigator.userAgent.match(/iPhone/i)
      return 'i'
    if navigator.userAgent.match(/Android/i)
      return 'a'
  getWellFormedDateString: (_digitString) ->
    tmp = _digitString.replace /-/g, ''
    rslt = tmp.substring 0,4
    rslt += '-'
    rslt += tmp.substring 4, 6
    rslt += '-'
    rslt += tmp.substring 6,8
    return rslt

  setConnectionPool: (status)->
    Meteor.setTimeout ->
      Meteor.call 'connectionPool', status, Router?.current()?.route?.name
    , 0

  isInt: (n) ->
#return boolean
    n % 1 is 0

  getCurrrentPath: ->
    c = window.location.pathname
    b = c.slice 0, -1
    a = c.slice -1
    if b is '' then return '/'
    else
      if a is '/' then return b
      else return c


  log: ->
#usage : Log().write('arg1', 'arg2')
    Log = Error
    Log.prototype.write = ->
      args = Array.prototype.slice.call arguments, 0
      suffix = if @lineNumber then 'line: ' + @lineNumber else 'stack: ' + @stack
      args.concat [suffix]

  isNumeric: (n) ->
    return !isNaN(parseFloat(n)) && isFinite(n)

  getClone: (_obj) ->
    JSON.parse(JSON.stringify(_obj));

  getClass: (obj) ->
    if typeof obj is "undefined" then return "undefined"
    if obj is null then return "null"
    return Object.prototype.toString.call(obj).match(/^\[object\s(.*)\]$/)[1]

  getStartEndOfDate: (_date) ->
    strYMD = mUtils.getStringYMDFromDate _date
    return rslt =
      start: mUtils.getDateFromString(strYMD + ' 00:00:00')
      end: mUtils.getDateFromString(strYMD + ' 00:00:00').addDates(1)

  getObjectCounts: (_object) ->
    Object.keys(_object).length

  formatNumber: (str) ->
    unless str? then return ''
    str = str.toString()
    num = str.replace(/\,/gi, '')
    num.toString().replace /(\d)(?=(\d{3})+(?!\d))/g, '$1,'

  hasOnlyDigits: (_val) ->
    if /^-?\d+\.?\d*$/.test _val then return true
    else return false

  formatStringToNumber: (str) ->
    unless str? then return ''
    num = str.replace(/\,/gi, '')
    parseInt(num)

  datePlusZeroWord: (_val) ->
    if _val.length < 2 then return '0' + _val
    else return _val

  stringToDateArray: (_str, _selector) ->
    arr = []
    temps = _str.split(_selector)
    temps.forEach (_string) ->
      arr.push new Date(_string.trim())
    return arr

  # get several times ago
  # Usage: timeSince new Date()
  # return: x minutes ago
  timeSince: (date) ->
    seconds = Math.floor((new Date - date) / 1000)
    interval = Math.floor(seconds / 31536000)
    if interval > 1
      return interval + ' years'
    interval = Math.floor(seconds / 2592000)
    if interval > 1
      return interval + ' months'
    interval = Math.floor(seconds / 86400)
    if interval > 1
      return interval + ' days'
    interval = Math.floor(seconds / 3600)
    if interval > 1
      return interval + ' hours'
    interval = Math.floor(seconds / 60)
    if interval > 1
      return interval + ' mins'
    return Math.floor(seconds) + ' seconds'





#below is trash

  #한반도 대분류
  대분류: (_cate) ->
    switch _cate.substring 0, 1
      when 'a' then return '소개'
      when 'b' then return '소식'
      when 'c' then return '학술'
      when 'd' then return '운동'
      when 'e' then return '나눔'


  등급조회: (_user) ->
    unless _user then throw new Meteor.Error '유저정보를 찾을수 없습니다. code: 1210'
    unless _user.profile.누적결제금액? then throw new Meteor.Error '누적결제금액 데이터가 없습니다. code: 1211'
    #    if _user.profile.누적결제금액 is 0 then return 0
    #    else
    result = parseInt(_user.profile.누적결제금액 / 1000000) + 1
    if result > 5 then result = 5
    return result

#마일리지 계산방식이 바뀔때를 대비하여 모든 마일리지는 이 함수를 호출.
  마일리지조회: (_user) ->
    unless _user then throw new Meteor.Error '유저정보를 찾을수 없습니다. code: 1212'
    unless _user.profile.마일리지? then throw new Meteor.Error '마일리지 정보를 찾을수 없습니다. code: 1213'

    return mUtils.formatNumber(_user.profile.마일리지) + ' p'

  마일리지지급: (_mileage) ->
    unless _mileage? then throw new Meteor.Error '마일리지 정보를 찾을수 없습니다. code: 1215'
    Meteor.call '마일리지지급', _mileage, (err, rslt) ->
      if err then alert err
      else return 'success'

  누적결제금액추가: (_user_id, _amount) ->
    unless _user_id? then return
    unless _amount? then return
    Meteor.call '누적결제금액추가', _user_id, _amount