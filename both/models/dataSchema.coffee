@dataSchema = (_objName, _addData) ->
  rslt = {}

  # add 될 데이터가 있다면 return 시에 extend 해서 반환한다.

  addData = _addData or {}

  switch _objName
    when 'banner'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        인덱스: ''
        이미지: ''
        링크: ''
    when 'popup'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        이미지: ''
        링크: ''
    when 'comment'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        작성자정보: {}
        댓글: ''
        게시물아이디: ''
    when 'board'
      rslt=
        createdAt: new Date()
        updatedAt: new Date()
        글번호: 0
        타이틀: ''
        내용: ''
        작성자정보: {}
        게시장소: '' #공지사항 / 글나눔터
        말머리: '' #'이야기숲', '추천도서','강좌후기', '모임후기', '행사후기' / ‘학당소식’, ‘강좌소식’, ‘행사소식’, ‘보도자료’ ‘영상자료’
        조회수: 0
        hasReadComment: true
    when 'profile'
      rslt =
        이름: ''
        연락처: ''
        이메일: ''
        성별: ''
        생년월일: ''
        직업: ''
        찾아온경로: ''
        비고: ''  #찾아온경로가 지인소개/기타 일경우 추가 입력하는 란
        회원등급: '일반' #default: 일반 / 관리자 / 강사
        비밀번호: ''
        facebookId: ''
        kakaoId: ''
        isDeletedAccounts: false
        강사정보: {
          직책: ''
          강사등급: ''
          커버연령: []
          강의구분: []
        }
        누적결제금액: 0
        마일리지: 0
    when 'program'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        시작일: ''
        종료일: ''
        수업일: []
        시작시간: ''
        종료시간: ''
        시간: ''
        타이틀: ''
        내용: ''
        모집인원: ''
        신청비: ''
        말머리: '말머리없음' #게시장소 예)[토론모임] / 말머리 없음 추가
        모집상태: '모집중' #모집중 / 마감임박 / 마감
        게시여부: true
        대분류: '모임강좌' #모임강좌 / 행사 / 커뮤니티
        비고: ''
        장소: ''
        대상: ''
        강사_id: ''   #담당 강사 user _id # 뭐지.. 따옴표 빼면 에러남
#        그룹이름: ''
        운영자: ''  #담당강사 이름
        연락처: ''  #담당강사 연락처
        문의: ''  #담당강사 이메일
        모임분류: '오프라인'  #온라인 / 오프라인
        조회수: 0
        내외부: '내부'   #내부 / 외부
        예산: 0
        강사등급: ''
        프로그램링크_id: ''   #programLink collection의 id
        정산여부: '미정산'  #미정산, 정산완료
        정산금액: 0       #강사 정산 되면 정산 금액이 입력 된다.
        후기: ''
    when 'event'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        일자: ''
        시간: ''
        타이틀: ''
        내용: ''
        모집인원: ''
        신청비: ''
        말머리: '' #게시장소 예)[토론모임]
        모집상태: '모집중' #모집중 / 마감임박 / 마감
        게시여부: true
        대분류: '행사' #모임강좌 / 행사 / 커뮤니티
        비고: ''
        장소: ''
        리스트이미지: ''
    when 'calendar'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        modifiedAt: []
        id: '' #lecture._id
        title: '' #lecture.강좌명
        url: '' #lecture detail url
        start: '' #date type or YYYY-MM-ddTHH:mm:ss
        end: ''
        color: '' #삼봉클럽 / 배우기 / 만들기 / 대관 / 공휴일 총 5개
        게시여부: false
        obj: {}
    when 'enrollment'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        게시물아이디: ''
        접수상태: '' #대기 / 완료 / 취소
        결제방법: '' #신용카드 / 무통장입금 / 실시간계좌이체
        신청인정보: {
          #신청인 user 정보
        }
        paymentId: ''
        etc: ''
    when 'payment'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        buyerInfo: {
          #결제자 user 정보
        }
        payInfo: {
          #결제 response
          #success: true
          #paid_amount: 금액
          #pay_method:  'card' : 신용카드 | 'trans' : 실시간계좌이체 | 'vbank' : 가상계좌 | 'phone' : 휴대폰소액결제 | etc : 기타/관리자입금 | lecturer: 강사입금
          #custom_data: {boardId / buyerId}
        }
#        mobilePay: false  #나중에 추가. 모바일결제시 true
    when 'userate'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        년월일: ''
        count: 1  #page view
        visitorCount: 1 #visitor count, basis of IP
    when 'connection'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        년월일: '' #yyyy-mm-dd
        connectionInfo: {}
        ###
          id: 'NEbPBpsApckcZYBjn',
          close: [Function],
          onClose: [Function],
          clientAddress: null,
          httpHeaders:
            {
              referer: 'http://1.255.55.240:9090/',
              'x-forwarded-for': '14.38.11.1',
              host: '1.255.55.240:9090',
              'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/57.0.2987.100 Mobile/14D27 Safari/602.1',
              'accept-language': 'ko-kr'
            }
        ###
    when 'rental'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        말머리: ''
        제목: ''
        이메일: ''
        내용: ''
        신청서파일: ''
        날짜: ''
        시작시간: ''
        종료시간: ''
        게시여부: false
        obj: {}
    when 'submitLecture'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        프로그램: ''  #jDefine.개설신청프로그램
        강의대상: ''  #jDefine.개설신청강의대상
        강의장소: ''
        수강인원: 0
        예산: ''
        지정도서: ''
        강의시간: ''
        강의일정: ''
        소속: ''
        이름: ''
        연락처: ''
        이메일: ''
        추가사항: ''
        설문작성: {}
        obj: {}
    when 'mileage'
      rslt =
        createdAt: new Date()
        updatedAt: new Date()
        user_id: '' #받은 유저 _id
        enrollment_id: '' #결제로 인한 발생시 _id 를, 관리자가 줄경우 'admin'
        title: '' #결제시 결제명, 관리자가 준경우 '관리자지급'
        reason: ''  #결제사용, 결제적립, 결제사용취소, 결제적립취소, 관리자가 지급한 이유
        amount: 0
        유저명: ''
    else
      throw new Error '### Data Schema Not found'

  return _.extend rslt, addData


#payment = {
#  _id:
#  status: '완료 취소 대기'
#  ourInfo: {
#   1.users
##    우리측 정보. 유저_id,
## 결제 정보 중에 결제사에서 알려주지 않는데 우리가 알고 있어야 하는 정보들
#
#  }
#  payInfo: {
##    금액 결제 id .....
#  }
#}